/* jshint node:true */
'use strict';

var ENV = 'dev';
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();

gulp.task('scss', function () {
    return gulp.src('app/newTab/**/*.scss')
        .pipe($.plumber())
        .pipe($.rubySass({
            style: 'expanded',
            precision: 10
        }))
        .pipe($.autoprefixer({browsers: ['last 1 version']}))
        .pipe(gulp.dest('.tmp/newTab'));
});

gulp.task('css', ['scss'], function () {
    return gulp.src('.tmp/newTab/**/*.css')
        .pipe(gulp.dest('dist/newTab/styles'));
});

gulp.task('js', function () {
  var jsTask = gulp.src('app/newTab/**/*.js')
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish'))
        .pipe($.jshint.reporter('fail'));
  //if (ENV == 'dev') {
  //  jsTask.pipe(gulp.dest('dist/newTab/scripts'))
  //}
  return jsTask;
});

gulp.task('html', ['js', 'css'], function () {
    var lazypipe = require('lazypipe');
    var cssChannel = lazypipe()
        .pipe($.csso)
        .pipe($.replace, 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap', 'fonts');
    var assets = $.useref.assets({searchPath: '{.tmp/newTab,app/newTab}'});

    var appFiles = gulp.src('app/newTab/*.html')
        .pipe(assets);
    if (ENV == 'prod') {
      appFiles.pipe($.if('*.js', $.uglify()));
    }
    appFiles
        .pipe($.if('*.css', cssChannel()))
        .pipe(assets.restore())
        .pipe($.useref())
        .pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
        .pipe(gulp.dest('dist/newTab'));
    return appFiles
});

gulp.task('rootFiles', function () {
  return gulp.src('app/*.*')
    .pipe(gulp.dest('dist/'));
});

gulp.task('images', function () {
    return gulp.src('app/newTab/**/*')
        .pipe($.filter('**/*.{png, jpg}'))
        .pipe($.cache($.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest('dist/newTab/images'));
});

gulp.task('fonts', function () {
    return gulp.src(require('main-bower-files')().concat('app/newTab/**/*'))
        .pipe($.filter('**/*.{eot,svg,ttf,woff}'))
        .pipe($.flatten())
        .pipe(gulp.dest('dist/newTab/fonts'));
});

gulp.task('htaccess', function () {
    return gulp.src([
      'node_modules/apache-server-configs/dist/.htaccess'
    ], { dot: true }).pipe(gulp.dest('dist'));
});

// inject bower components
gulp.task('wiredep', function () {
    var wiredep = require('wiredep').stream;

    gulp.src('app/newTab/*.scss')
        .pipe(wiredep())
        .pipe(gulp.dest('app/newTab'));

    gulp.src('app/newtab/*.html')
        .pipe(wiredep({exclude: ['bootstrap-sass-official']}))
        .pipe(gulp.dest('app/newTab'));
});

//gulp.task('connect', ['styles'], function () {
//    var serveStatic = require('serve-static');
//    var serveIndex = require('serve-index');
//    var app = require('connect')()
//        .use(require('connect-livereload')({port: 35729}))
//        .use(serveStatic('.tmp'))
//        .use(serveStatic('app'))
//        // paths to bower_components should be relative to the current file
//        // e.g. in app/index.html you should use ../bower_components
//        .use('/bower_components', serveStatic('bower_components'))
//        .use(serveIndex('app'));
//
//    require('http').createServer(app)
//        .listen(9000)
//        .on('listening', function () {
//            console.log('Started connect web server on http://localhost:9000');
//        });
//});

//gulp.task('serve', ['connect', 'watch'], function () {
//    require('opn')('http://localhost:9000');
//});
//

//gulp.task('watch', ['connect'], function () {
//	$.livereload.listen();
//
//	// watch for changes
//	gulp.watch([
//		'app/*.html',
//		'.tmp/**/*.css',
//		'app/**/*.js',
//		'app/**/*'
//	]).on('change', $.livereload.changed);
//
//	gulp.watch('app/**/*.scss', ['styles']);
//	gulp.watch('bower.json', ['wiredep']);
//});

gulp.task('clean', function () {
  return gulp.src(['.tmp', 'dist'], {read: false})
    .pipe($.clean());
});

gulp.task('build', ['html', 'images', 'fonts', 'htaccess', 'rootFiles'], function () {
    return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});